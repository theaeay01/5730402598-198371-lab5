<?php
// create the root node of DOM tree
$fileName = "myProfile.xml";
$doc = new DomDocument('1.0');
// create element 'nation'
$root = $doc->createElement('profile');
// assign the element 'nation' as the root node of the XML doc
$doc->appendChild($root);
$name = $doc->createElement('name');
$nameValue = $doc->createTextNode('Kittipong Phinyo');
$name->appendChild($nameValue);
$location = $doc->createElement('major');
$locationValue = $doc->createTextNode('Computer Engineering');
$area = $doc->createElement('area');
$areaValue = $doc->createTextNode('Software');
// make the text node be the chold of node 'area'
$area->appendChild($areaValue);
// make the text node be the chold of node 'location'
$location->appendChild($locationValue);
$location->appendChild($area);
// make the element node 'location' be the child of root node of XML doc
$root->appendChild($location);
// make the element node 'name' be the child of the root node of XML doc
$root->appendChild($name);
// set attribute of the root node
$root->setAttribute('id', '5730402598');
// assign the element 'nation' as the root node of the XML doc
$doc->appendChild($root);
// save DOM tree in an XML file
//$str = $doc->saveXML();
$doc->save($fileName);
echo "Writing XML File Complete in name:$fileName";
?>